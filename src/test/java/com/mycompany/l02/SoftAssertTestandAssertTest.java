package com.mycompany.l02;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static org.testng.Assert.*;
import static org.testng.Assert.assertEquals;

public class SoftAssertTestandAssertTest extends TestBase {


    String nameData = "141007098";

    @Test
    public void softAssertTest() {
        SoftAssert softAssert = new SoftAssert();
        WebElement searchField = driver.findElement(By.name("search"));
        typeText(searchField, "141007098");
        WebElement itemPrice = driver.findElement(By.xpath("//span[contains(text(),'336')]"));
        itemPrice.click();
        WebElement itemColour = driver.findElement(By.xpath("//span[contains(text(),'336')]"));
        itemColour.click();
        WebElement itemBrand = driver.findElement(By.xpath("//div[@data-widget='webBrand']"));
        itemBrand.click();

        softAssert.assertTrue(itemPrice.isDisplayed());
        softAssert.assertTrue(itemColour.isDisplayed());
        softAssert.assertTrue(itemBrand.isDisplayed());

        softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(text(),'336')]"))
                .getText().contains("336"));

        softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(text(),'336')]"))
                .getCssValue("color").contains("249, 17, 85"));

        softAssert.assertTrue(driver.findElement(By.xpath("//div[@data-widget='webBrand']"))
                .getText().contains("Faber-Castell"));

        softAssert.assertAll();
    }



    public void typeText(WebElement element, String text) {
        element.click();
        element.clear();
        element.sendKeys(text);

    }
    @Test
    public void AssertTest() {
        //Printing Id of the thread on using which test method got executed
        System.out.println("Test Case One with Thread Id:- "
                + Thread.currentThread().getId());
        WebElement searchField = driver.findElement(By.name("search"));
        typeText(searchField, "141007098");
        WebElement itemPrice = driver.findElement(By.xpath("//span[contains(text(),'336')]"));
        WebElement itemColour = driver.findElement(By.xpath("//span[contains(text(),'336')]"));
        WebElement itemBrand = driver.findElement(By.xpath("//div[@data-widget='webBrand']"));
        assertTrue(itemPrice.getText().contains("336"));
        assertEquals(itemColour.getCssValue("color"), "249, 17, 85");
        assertEquals(itemBrand.getText(), "Faber-Castell");
    }
}
