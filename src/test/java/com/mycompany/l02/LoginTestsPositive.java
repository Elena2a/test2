package com.mycompany.l02;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;

public class LoginTestsPositive extends TestBase {


    @Test
    public void loginPositive() {
        //Printing Id of the thread on using which test method got executed
        System.out.println("Test Case One with Thread Id:- "
                + Thread.currentThread().getName());

        WebElement itemName = driver.findElement(By.name("search"));
        itemName.clear();
        itemName.sendKeys("141007098");


        driver.findElement(By.xpath("//div[@data-widget='webBrand']")).click();

        assertFalse(isElementPresent(By.xpath("//a[@class = \"a2g0 tile-hover-target\"]")));

    }

}
