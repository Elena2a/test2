package com.mycompany.l02;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;

public class BasicCommandsTest extends TestBase {



    String itemData = "141007098";

    @Test
    public void basicCommandsTest() {
        WebElement itemPrice = driver.findElement(By.xpath("//span[contains(text(),'336')]"));
        WebElement itemColour= driver.findElement(By.xpath("//span[contains(text(),'336')]"));
        WebElement itemBrand = driver.findElement(By.xpath("//div[@data-widget='webBrand']"));



        itemPrice.clear();
        itemPrice.sendKeys(itemData);


        System.out.println("itemButton price is  " + itemPrice.getText());
        System.out.println("itemname brand is   " + itemPrice.getAttribute("brand"));
        System.out.println("itemname class is  " + itemColour.getCssValue("background-color"));

        itemColour.click();

        assertFalse(isElementPresent(By.xpath("//span[@class=\"a0y4 a0y5\"]")));

    }


    public void typeText(WebElement element, String text) {
        element.click();
        element.clear();
        element.sendKeys(text);

    }

}
